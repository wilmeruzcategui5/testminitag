import { Component, OnInit } from '@angular/core';
import { EndpointService } from '../../services/endpoint.service';

@Component({
  selector: 'app-tabla1',
  templateUrl: './tabla1.component.html',
  styleUrls: ['./tabla1.component.css']
})
export class Tabla1Component implements OnInit {

  numeros: number[] = [];
  loading = true;
  indices: number[] = [];
  cantidad: any[] = [];
  primer: any[] = [];
  ultimo: any[] = [];
  error = false;
  valor = '';

  constructor(private servicio: EndpointService) {}

  ngOnInit() {
    this.servicio.obtenerarray().then( (resp: IRemoteResponse) => {

      // Si la respuesta es correcta verifica los numeros
      if (resp.success) {

        this.numeros = resp.data;
        this.valor = this.numeros.toString();

        // Obtenemos la lista de numeros unicas sin repetir
        this.indices = this.numeros.filter( (value, index, self) => {
          return self.indexOf(value) === index;
        });

        // Cuenta la cantidad de elementos por indice
        this.indices.forEach(element => {
          this.cantidad[element] = this.numeros.filter(x => x === element).length;
          this.primer[element] = this.numeros.indexOf(element);
          this.ultimo[element] = this.numeros.lastIndexOf(element);
        });

      }
      console.log(resp);
    }).catch(err => {
      this.error = true;
      console.log(err);
      console.log('Ocurrió un error');
    }).finally(() => {
      this.loading = false;
    });
  }

  // Ordenado con el metodo burbuja
  ordenar() {

    let k: number;
    let aux: number;
    let i: number;
    for (k = 1; k < this.numeros.length; k++) {
        for (i = 0; i < (this.numeros.length - k); i++) {
            if (this.numeros[i] > this.numeros[i + 1]) {
                aux = this.numeros[i];
                this.numeros[i] = this.numeros[i + 1];
                this.numeros[i + 1] = aux;
            }
        }
    }
    this.valor = this.numeros.toString();
  }

}

export interface IRemoteResponse {
  data?: any;
  error?: any;
  success: boolean;
}
