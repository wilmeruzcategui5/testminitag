import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  urlEndpoint = 'http://168.232.165.184/prueba/array';

  constructor(private http: HttpClient) {
    console.log('Servicio funcionando');
  }

  obtenerarray() {
    return this.http.get(this.urlEndpoint).toPromise();
  }

}
