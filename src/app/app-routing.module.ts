import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Tabla1Component } from './tablas/tabla1/tabla1.component';
import { Tabla2Component } from './tablas/tabla2/tabla2.component';

const routes: Routes = [
  // { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'tabla1',
    component: Tabla1Component,
  },
  {
    path: 'tabla2',
    component: Tabla2Component,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
